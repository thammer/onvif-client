## 设备发现

### 1. 下载wsdl文件
从onvif官网下载[remotediscovery.wsdl](https://www.onvif.org/onvif/ver10/network/wsdl/remotediscovery.wsdl) 文件。
在官网的网络接口规范里面是找不到链接的，因为onvif没定义设备发现的profile，而是复用了已经很成熟的WS-Discovery标准，WS-Discovery 协议
使得服务能够被客户端发现。

### 2. 使用gsoap生成相关的文件
```shell script
wsdl2h -o remotediscovery.h -c -s remotediscovery.wsdl
```
也可以直接跟文件的链接生成
```shell script
wsdl2h -o remotediscovery.h -c -s https://www.onvif.org/onvif/ver10/network/wsdl/remotediscovery.wsdl
```
生成c文件
```shell script
soapcpp2 -2 -C -c -x -I/usr/share/gsoap/import -I/usr/share/gsoap/custom remotediscovery.h
```
最终生成了如下文件
```text
-rw-r--r-- 1 thomas thomas    852 4月  27 18:26 RemoteDiscoveryBinding.nsmap
-rw-r--r-- 1 thomas thomas  21402 4月  27 18:24 remotediscovery.h
-rw-r--r-- 1 thomas thomas 229050 4月  27 18:26 soapC.c
-rw-r--r-- 1 thomas thomas  21096 4月  27 18:26 soapClient.c
-rw-r--r-- 1 thomas thomas    923 4月  27 18:26 soapClientLib.c
-rw-r--r-- 1 thomas thomas 156284 4月  27 18:26 soapH.h
-rw-r--r-- 1 thomas thomas  42752 4月  27 18:26 soapStub.h
-rw-r--r-- 1 thomas thomas    852 4月  27 18:26 wsdd.nsmap
```
保留如下文件即可，*.nsmap文件内容都一样，保留一个即可。
```text
-rw-r--r-- 1 thomas thomas 229050 4月  27 18:26 soapC.c
-rw-r--r-- 1 thomas thomas  21096 4月  27 18:26 soapClient.c
-rw-r--r-- 1 thomas thomas 156284 4月  27 18:26 soapH.h
-rw-r--r-- 1 thomas thomas  42752 4月  27 18:26 soapStub.h
-rw-r--r-- 1 thomas thomas    852 4月  27 18:26 wsdd.nsmap
```
### 3. 从gsoap安装目录拷贝几个文件
从gsoap安装目录拷贝几个文件
```shell script
cp /usr/share/gsoap/plugin/wsaapi.* ./
```
客户端设备发现测试程序即源码里面的`main.c`,编译的时候需要链接libgsoap库。

### 4. 编译测试