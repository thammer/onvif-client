## 设备信息获取

### 1. wsdl2h生成h文件
参考前面

### 2. 加入鉴权依赖的头文件
####修改生成头文件
在生成的头文件haha.h中加入 `#import "wsse.h"`,另外再次修改`#import "wsdd10.h"`为`#import "wsdd5.h"`
```text
#import "wsse.h" 
#import "wsdd5.h"       
#import "xop.h" 
#import "wsa5.h"
```
####生成c文件
```shell script
soapcpp2 -2 -C -c -x -I/usr/share/gsoap haha.h
```
####删除中间文件
删除多余的内容一样的*.nsmap文件，保留一个即可`wsdd.nsmap`,删除头文件`haha.h`,`soapClientLib.c`

### 4. 通过直接修改CMakeLists.txt文件，避免拷贝
新增鉴权需要的几个插件，wsseapi.c、wsseapi.h、mecevp.c、smdevp.c、threads.c，这些文件在gsoap目录和gsoap/plugin目录下。
编译过程中`mecevp.c smdevp.c`会报有几个openssl库里面的函数隐式声明，在对应的头文件加上#include即可。还会报调用废弃函数，不管。

### 5. 编译测试
```text
生产商:HIKVISION
模块:DS-2CD7187HWD-IZ
固件版本:V5.5.73 build 190131
序列号:DS-2CD7187HWD-IZ20190516AACHD20271711
硬件ID:88
[soap] GetDeviceInformation error: 12, SOAP-ENV:Sender, The action requested requires authorization and the sender is not authorized
```
我的环境中有两个摄像头，另一个就鉴权失败了。
