## 设备信息获取

### 1. 下载wsdl文件
从onvif官网下载下面几个文件,入口在这[网络接口规范](https://www.onvif.org/ch/profiles/specifications/)， 进去后下载。

[remotediscovery.wsdl](https://www.onvif.org/onvif/ver10/network/wsdl/remotediscovery.wsdl) 

[devicemgmt.wsdl](https://www.onvif.org/ver10/device/wsdl/devicemgmt.wsdl)

[onvif.xsd](https://www.onvif.org/ver10/schema/onvif.xsd)

[common.xsd](https://www.onvif.org/ver10/schema/common.xsd)

和前面`remotediscovery.wsdl`设备发现不同的是，`devicemgmt.wsdl`这里需要几个xsd文件。wsdl文件里面会用到xsd文件，可以理解为c文件和h
文件的关系即可。有些c文件的不需要h文件就可以编译，如同这里的remotediscovery.wsdl文件不需要依赖xsd文件一样。

### 2. 修改devicemgmt.wsdl
打开`devicemgmt.wsdl`文件，修改14行
```xml
<xs:import namespace="http://www.onvif.org/ver10/schema" schemaLocation="../../../ver10/schema/onvif.xsd"/>
```
修改为
```xml
<xs:import namespace="http://www.onvif.org/ver10/schema" schemaLocation="你自己本地下载的文件路径/onvif.xsd"/>
```
这就好比头文件路径改了，你要么制定新的头文件搜索路径，要么include指明绝对路径。

### 3. 使用gsoap生成相关的文件
使用本地文件生成
```shell script
wsdl2h -o haha.h remotediscovery.wsdl devicemgmt.wsdl onvif.xsd common.xsd
```
也可以直接跟文件的链接生成，这种方法就不用显示下载xsd文件，也不用改wsdl文件，`wsdl2h`工具会自动推导下载各种依赖的xsd文件。
```shell script
wsdl2h -o haha.h -c -s https://www.onvif.org/onvif/ver10/network/wsdl/remotediscovery.wsdl https://www.onvif.org/ver10/device/wsdl/devicemgmt.wsdl
```
生成c文件
```shell script
soapcpp2 -2 -C -c -x -I/usr/share/gsoap haha.h
```
在生成文件过程中有一个错误
```text
wsa5.h(288): ERROR: remote method name clash: struct/class 'SOAP_ENV__Fault' already declared at line 274
```
google之，sourceforge上有人遇到类似问题，https://sourceforge.net/p/gsoap2/bugs/999/
即生成的onvif.h文件中100行左右会做如下动作
```c
  100 #import "wsdd10.h"       // wsdd10 = <http://schemas.xmlsoap.org/ws/2005/04/discovery>
  101 #import "xop.h" // xop = <http://www.w3.org/2004/08/xop/include>
  102 #import "wsa5.h"        // wsa5 = <http://www.w3.org/2005/08/addressing>
```
而`wsdd10.h`这个文件中33行又会
```c
  33 #import "wsa.h" // wsa = <http://schemas.xmlsoap.org/ws/2004/08/addressing>
```
`wsa.h` `wsa5.h` 这两个文件应该是不同的版本，他们里面声明的函数冲突了。所以将`onvif.h`100行改为下面这样即可
```c
100 #import "wsdd5.h"       // wsdd10 = <http://schemas.xmlsoap.org/ws/2005/04/discovery>
```


### 4. 通过直接修改CMakeLists.txt文件，避免拷贝
```cmake
cmake_minimum_required(VERSION 3.10)

project(ONVIF_CLEINT C)

set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -O0 -g -ggdb -Wall")

include_directories(${PROJECT_SOURCE_DIR}/
					/usr/share/gsoap/plugin/
					/usr/share/gsoap/custom/)

set(GSOAP_SRCS 
	./soapC.c
	./soapClient.c
	/usr/share/gsoap/plugin/wsaapi.c
	/usr/share/gsoap/custom/duration.c)
	
set(CLIENT_SRCS ${GSOAP_SRCS}
	./main.c)

add_executable(client ${CLIENT_SRCS}) 

target_link_libraries(client gsoap)
```

客户端设备信息获取测试程序即源码里面的`main.c`,编译的时候需要链接libgsoap库。

### 5. 编译测试
编译过程卡半天，可能是自动生成的代码比较复杂，好像单个文件几万行。单个soapC.c文件就是17万行，尼玛！！
```text
./client
[soap] GetDeviceInformation error: 401, detected, HTTP/1.1 401 Unauthorized
```
这是因为在通过http请求获取设备信息的时候，我的摄像头要求要验证，进摄像头后台关了即可。