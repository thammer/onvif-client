## 获取设备能力

### 参考[onvif系列教程](https://blog.csdn.net/benkaoya/article/details/72486544)

版权声明：本文为CSDN博主「许振坪」的原创文章，遵循 CC 4.0 BY-SA 版权协议，转载请附上原文出处链接及本声明。
原文链接：https://blog.csdn.net/benkaoya/java/article/details/72486544

## 问题
有点需要注意的是如果摄像头支持一些扩展的功能，例如海康这个：
```xml
<tt:Extension><hikxsd:hikCapabilities><hikxsd:XAddr>http://192.168.0.35/onvif/hik_ext</hikxsd:XAddr>
<hikxsd:IOInputSupport>true</hikxsd:IOInputSupport>
<hikxsd:PrivacyMaskSupport>true</hikxsd:PrivacyMaskSupport>
<hikxsd:PTZ3DZoomSupport>false</hikxsd:PTZ3DZoomSupport>
<hikxsd:PTZPatternSupport>true</hikxsd:PTZPatternSupport>
<hikxsd:Language>1</hikxsd:Language>
</hikxsd:hikCapabilities>
<tt:DeviceIO><tt:XAddr>http://192.168.0.35/onvif/DeviceIO</tt:XAddr>
<tt:VideoSources>1</tt:VideoSources>
<tt:VideoOutputs>0</tt:VideoOutputs>
<tt:AudioSources>1</tt:AudioSources>
<tt:AudioOutputs>1</tt:AudioOutputs>
<tt:RelayOutputs>1</tt:RelayOutputs>
</tt:DeviceIO>
<tt:Recording><tt:XAddr>http://192.168.0.35/onvif/Recording</tt:XAddr>
<tt:ReceiverSource>false</tt:ReceiverSource>
<tt:MediaProfileSource>true</tt:MediaProfileSource>
<tt:DynamicRecordings>false</tt:DynamicRecordings>
<tt:DynamicTracks>false</tt:DynamicTracks>
<tt:MaxStringLength>64</tt:MaxStringLength>
</tt:Recording>
<tt:Search><tt:XAddr>http://192.168.0.35/onvif/SearchRecording</tt:XAddr>
<tt:MetadataSearch>false</tt:MetadataSearch>
</tt:Search>
<tt:Replay><tt:XAddr>http://192.168.0.35/onvif/Replay</tt:XAddr>
</tt:Replay>
</tt:Extension>
```
里面定义了海康自己的扩展字段，会出现抓包看回复是正常，但是解析失败的现象，返回`SOAP_TYPE`这种错误。解决的办法就是，生成头文件时，使用
wsdl2h的`-d`或者`-x`选项，参考 [StackOverflow](https://stackoverflow.com/questions/34019860/onvif-with-gsoap-status-error-4-soap-type) :
```shell script
wsdl2h -o haha.h -s -c -x remotediscovery.wsdl devicemgmt.wsdl onvif.xsd common.xsd
```
wsdl2h -h:
```text
-d      use DOM to populate xs:any, xs:anyType, and xs:anyAttribute
-x      don't generate _XML any/anyAttribute extensibility elements
```

未做深入的查证，推测： 
> -d 使用DOM填入 xs:any, xs:anyType, and xs:anyAttribute 这些地方，DOM可能描述私有扩展字段是如何构成。
> -x 不生成any/anyAttribute extensibility这些元素，就是直接不解析私有扩展字段。

