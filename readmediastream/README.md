## 获取设备能力

参考[onvif系列教程](https://blog.csdn.net/benkaoya/article/details/72486544)

版权声明：本文为CSDN博主「许振坪」的原创文章，遵循 CC 4.0 BY-SA 版权协议，转载请附上原文出处链接及本声明。
[原文链接](https://blog.csdn.net/benkaoya/java/article/details/72486544)

## 使用新增的wsdl文件
本demo由于需要获取音视频信息，所以需要用到新的wsdl,[media.wsdl](https://www.onvif.org/ver10/media/wsdl/media.wsdl) ,它的下面还
一个`media2.wsdl`,不知道这两者什么关系，暂时只用`media.wsdl`，这里还会用到前面几个wsdl文件，`median.wsdl`里面也有个对`onvif.xsd`
引用路径的问题，按照之前的方法修改即可。

## 安装ffmpeg相关的库
```text
libavresample3:amd64                               2020-04-29 20:46:17.661637994 +0800
libbs2b0:amd64                                     2020-04-29 20:46:17.697639228 +0800
libflite1:amd64                                    2020-04-29 20:46:18.797676935 +0800
libmysofa0:amd64                                   2020-04-29 20:46:18.845678580 +0800
librubberband2:amd64                               2020-04-29 20:46:18.897680363 +0800
libnorm1:amd64                                     2020-04-29 20:46:18.965682693 +0800
libpgm-5.2-0:amd64                                 2020-04-29 20:46:19.017684477 +0800
libzmq5:amd64                                      2020-04-29 20:46:19.073686396 +0800
libavfilter6:amd64                                 2020-04-29 20:46:19.193690510 +0800
libopenal-data                                     2020-04-29 20:46:19.241692156 +0800
libopenal1:amd64                                   2020-04-29 20:46:19.301694212 +0800
libsdl2-2.0-0:amd64                                2020-04-29 20:46:19.377696817 +0800
libavdevice57:amd64                                2020-04-29 20:46:19.425698462 +0800
libavresample-dev:amd64                            2020-04-29 20:46:19.469699970 +0800
libpostproc-dev:amd64                              2020-04-29 20:46:19.513701479 +0800
libavfilter-dev:amd64                              2020-04-29 20:46:19.653706278 +0800
libavdevice-dev:amd64                              2020-04-29 20:46:19.705708060 +0800
```
安装开发库，带-dev的那几个，也许不全，根据链接错误提示逐个排查即可。

## 编译测试